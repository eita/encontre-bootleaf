var map, featureList, boroughSearch = [], theaterSearch = [], museumSearch = [], dataResponsaSearch = [];
var featureInfoHTML = {};
/*var iconFonts = 
	{
	'conheca':{icon:'book', bkgcolor:'darkgreen', color:'#fff'},
	// 'conheca':{icon:'eye', bkgcolor:'darkgreen', color:'#fff'},
	'mao-na-massa':{icon:'group', bkgcolor:'orange', color:'#333'},
	'compre-e-troque':{icon:'gift', bkgcolor:'red', color:'#fff'},
	// 'compre-e-troque':{icon:'shopping-cart', bkgcolor:'red', color:'#fff'},
	'servicos':{icon:'wrench', bkgcolor:'cadetblue', color:'#fff'},
	'recicle':{icon:'recycle', bkgcolor:'purple', color:'#333'}
	};
var iconFonts = 
		{
		'feiras':{icon:'shopping-basket', bkgcolor:'darkgreen', color:'#fff'},
		'grupos-de-consumo-responsavel':{icon:'group', bkgcolor:'orange', color:'#333'},
		'iniciativas-de-economia-solidaria':{icon:'gift', bkgcolor:'red', color:'#fff'},
		'iniciativas-de-agroecologia':{icon:'leaf', bkgcolor:'cadetblue', color:'#fff'},
		'restaurantes':{icon:'cutlery', bkgcolor:'purple', color:'#333'},
		};
*/

$(window).resize(function() {
  sizeLayerControl();
});

if( window.self !== window.top ) {
    $("#main-menu-nav, #branding").hide();
    $("#map").css("top",0);
}

if ($("#mailSentModal")) {
	$("#mailSentModal").modal("show");
}

if (sidebarIsEnabled) {
	$(document).on("click", ".feature-row", function(e) {
		$(document).off("mouseout", ".feature-row", clearHighlight);
		sidebarClick(parseInt($(this).attr("id"), 10));
	});

	$(document).on("mouseover", ".feature-row", function(e) {
		highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
	});

	$(document).on("mouseout", ".feature-row", clearHighlight);
}

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#aboutModal").on('click',"a[data-window='external']", function() {
	window.open($(this).attr('href')); 
	return false; 
});

$("#full-extent-btn").click(function() {
  map.fitBounds(boroughs.getBounds());
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
	toggleSidebar();
  return false;
});

//TODO: ver como o sidebar pode voltar a aparecer, quando enabled.
function toggleSidebar() {
	if ($('#sidebar').is(":visible")) {
		$('#sidebar').hide();
	} else {
		$('#sidebar').show();
	}
  map.invalidateSize();
}

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

if (sidebarIsEnabled) {
	$("#sidebar-toggle-btn").click(function() {
		toggleSidebar();
		return false;
	});

	$("#sidebar-hide-btn").click(function() {
		$('#sidebar').hide();
		map.invalidateSize();
	});
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar(targetCategory) {
	if (!sidebarIsEnabled) {
		return;
	}
	/* Loop through dataResponsa layer and add only features which are in the map bounds */
	$.each(dataResponsaLayers, function(category, respLayer) {
		dataResponsa[category].eachLayer(function (layer) {
			if (map.hasLayer(respLayer) && map.getBounds().contains(layer.getLatLng())) {
			    $("#"+L.stamp(layer)).show();
			} else {
			  	$("#"+L.stamp(layer)).hide();
			}
		});
	});
}

function syncSidebar_bckp() {
  /* Empty sidebar features */
  $("#feature-list tbody").empty();
  /* Loop through dataResponsa layer and add only features which are in the map bounds */
  var featureName = "";
  $.each(dataResponsaLayers, function(category, respLayer) {
		dataResponsa[category].eachLayer(function (layer) {
		  if (map.hasLayer(respLayer)) {
		    if (map.getBounds().contains(layer.getLatLng())) {
		      featureName = layer.feature.properties.title + "<p class='location'>" + layer.feature.properties.city + "/" + layer.feature.properties.state + "</p>";
		      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td class="feature-name">' + featureName + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
		    }
		  }
		});
	});
  /* Update list.js featureList */

  featureList = new List("features", {
    valueNames: ["feature-name"]
  });
  featureList.sort("feature-name", {
    order: "asc"
  });
}

/* Basemap Layers */
var defaultBaseMap = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    	maxZoom: 19,
    	zoomControl: false,
    	id: 'dtygel.mk0l1gn6',
    	accessToken: 'pk.eyJ1IjoiZHR5Z2VsIiwiYSI6IjY4YjRhNjU1NWJhYTU2OGU1YWQ1OTcxYjRjNDcyNzIxIn0.Go-whnL9yIt3rHicQ5gqbA'
});
var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
var mapquestOSM = L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png", {
  maxZoom: 19,
  subdomains: ["otile1", "otile2", "otile3", "otile4"],
  attribution: 'Tiles courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});
var mapquestOAM = L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg", {
  maxZoom: 18,
  subdomains: ["oatile1", "oatile2", "oatile3", "oatile4"],
  attribution: 'Tiles courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a>. Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency'
});
var mapquestHYB = L.layerGroup([L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg", {
  maxZoom: 18,
  subdomains: ["oatile1", "oatile2", "oatile3", "oatile4"]
}), L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/hyb/{z}/{x}/{y}.png", {
  maxZoom: 19,
  subdomains: ["oatile1", "oatile2", "oatile3", "oatile4"],
  attribution: 'Labels courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA. Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency'
})]);

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.7,
  radius: 10
};

/* Single marker cluster layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: true,
  zoomToBoundsOnClick: true,
  disableClusteringAtZoom: 11,
  maxClusterRadius: 20
});

/* Empty layer placeholder to add to layer control for listening when to add/remove initiatives to markerClusters layer */
var dataResponsaLayers = {};
var dataResponsa = {};
var categoryMarkerIcons = {};
$.each(categoryIcons, function(category, iconUrl) {
	categoryMarkerIcons[category] = L.icon({
		iconUrl: 'assets/img/'+iconUrl,
		iconSize: [36, 36]
	});
});
var finalDataDownloadUrl = (selectedCategories && !getAllData)
    ? dataDownloadUrl + "?categories="+selectedCategories.join(",")+"&callback=?"
    : dataDownloadUrl + "?callback=?";
$.getJSON(finalDataDownloadUrl, function (data) {
	//var iconAwesome = "";
	$.each(data, function(category, items) {
	    if (items.features.length==0) {
	        return true;
	    }
		dataResponsaLayers[category] = L.geoJson(null);
		/*if (typeof(iconFonts[category])=='undefined') {
			console.log('Error. Category "'+category+'" is not configured.')
		}
		iconAwesome = L.AwesomeMarkers.icon({
			icon: iconFonts[category].icon, 
			prefix: 'fa', 
			markerColor: iconFonts[category].bkgcolor, 
			iconColor: iconFonts[category].color
		});*/
		dataResponsa[category] = L.geoJson(null, {
			pointToLayer: function (feature, latlng) {
				return L.marker(latlng, {
				  icon: categoryMarkerIcons[category],
				  title: feature.properties.title,
				  riseOnHover: true
				});
			},
			onEachFeature: function (feature, layer) {
				if (feature.properties) {
				  layer.on({
				    click: function (e) {
				      $("#feature-title span").html(feature.properties.title);
				      $("#feature-info").html("");
				      saveUrl(layer.feature.properties.id);
				      openFeatureInfoModal(layer.feature.properties.id, feature.geometry.coordinates[1], feature.geometry.coordinates[0]);
				    }
				  });
				  dataResponsaSearch.push({
				    name: layer.feature.properties.title,
				    location: layer.feature.properties.city+'/'+layer.feature.properties.state,
				    source: "Iniciativas",
				    id: L.stamp(layer),
				    lat: layer.feature.geometry.coordinates[1],
				    lng: layer.feature.geometry.coordinates[0]
				  });
				  if (sidebarIsEnabled) {
				  	featureName = layer.feature.properties.title + "<p class='location'>" + layer.feature.properties.city + "/" + layer.feature.properties.state + "</p>";
		      	$("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td class="feature-name">' + featureName + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
				  }
				}
			}
		});
	
      	dataResponsa[category].addData(items);
      	if (!selectedCategories || selectedCategories.indexOf(category)>-1) {
      	    map.addLayer(dataResponsaLayers[category]);
      	    markerClusters.addLayer(dataResponsa[category]);
      	}
    });
	$.each(dataResponsaLayers, function(category,layer) {
		groupedOverlays.Iniciativas[category] = layer;
	});
	if (Object.keys(dataResponsaLayers).length>1) {
	    layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
		    collapsed: isCollapsed
	    }).addTo(map);
	}
});

if (typeof currentLocation=='undefined') {
	var currentLocation = {lat:-15.7797200, lng:-47.9297200};
}
if (typeof currentZoom=='undefined') {
	var currentZoom = 4;
}
map = L.map("map", {
  zoom: currentZoom,
  center: [currentLocation.lat, currentLocation.lng],
  layers: [OpenStreetMap_Mapnik, markerClusters, highlight],
  zoomControl: false,
  attributionControl: false
});


/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
	$.each(dataResponsaLayers, function(category,layer) {
		if (e.layer === layer) {
    	markerClusters.addLayer(dataResponsa[category]);
    	syncSidebar(category);
  	}
	});
});

map.on("overlayremove", function(e) {
	$.each(dataResponsaLayers, function(category,layer) {
		if (e.layer === layer) {
    	markerClusters.removeLayer(dataResponsa[category]);
    	syncSidebar(category);
  	}
	});
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
  saveUrl();
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

function saveUrl(uniqueId) {
	currentLocation = map.getCenter();
	currentZoom = map.getZoom();
	var newUrl = '?lat='+currentLocation.lat+'&lng='+currentLocation.lng+'&zoom='+currentZoom;
	if (typeof uniqueId != 'undefined' && uniqueId) {
		newUrl += '&id='+uniqueId;
	}
	window.history.pushState(newUrl, 'Encontre', newUrl);
}

function openFeatureInfoModal(id, lat, lng) {
	if (typeof featureInfoHTML[id] != 'undefined') {
		$("#featureModal").modal("show");
		$("#feature-title img").attr('src', 'assets/img/'+categoryIcons[featureInfoHTML[id].category]);
		$("#feature-span-title").html(featureInfoHTML[id].title);
		$("#feature-span-territory").html(featureInfoHTML[id].territory_label);
		$("#feature-info").html(featureInfoHTML[id].html);
		highlight.clearLayers().addLayer(L.circleMarker([featureInfoHTML[id].lat, featureInfoHTML[id].lng], highlightStyle));
	} else {
		$("#loading").show();
		$.getJSON("https://dtygel.vps2.eita.org.br/responsa_datamining/ajax_get_single_initiative_popup_html.php?unique_id="+id+"&callback=?", function (info) {
			console.log(info.category);
			$("#featureModal").modal("show");
			featureInfoHTML[id] = {title:info.title, territory_label: info.territory_label, category: info.category, html:info.html, lat:info.lat, lng:info.lng};
			$("#feature-title img").attr('src', 'assets/img/'+categoryIcons[info.category]);
			$("#feature-span-title").html(info.title);
			$("#feature-span-territory").html(info.territory_label);
			$("#feature-info").html(info.html);
			$('#loading').hide();
			highlight.clearLayers().addLayer(L.circleMarker([info.lat, info.lng], highlightStyle));
		});
	}
}

function makeReadableName(cat) {
	return "<img class='categoryIcon' src='assets/img/"+categoryIcons[cat]+"'>" + categoryNames[cat];
}

var attributionControl = L.control({
  position: "bottomright"
});
attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "<a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Créditos da plataforma</a><br />Mapa: © contribuidores do <a href='http://openstreetmap.org'>OpenStreetMap</a>";
  return div;
};
map.addControl(attributionControl);

var zoomControl = L.control.zoom({
  position: "topleft"
}).addTo(map);

/* Show/Hide sidebar control */
if (sidebarIsEnabled) {
	var stateChangingButton = L.easyButton({
		states: [{
		  stateName: 'hide-list',
		  icon: 'fa-outdent',
		  title: 'Ocultar lista',
		  onClick: function(btn, map) {
		    toggleSidebar();
		    btn.state('show-list');
		  }
		}, {
		  stateName: 'show-list',
		  icon: 'fa-indent',
		  title: 'Mostrar lista',
		  onClick: function(btn, map) {
		    toggleSidebar();
		    btn.state('hide-list');
		  }
		}]
	});
	stateChangingButton.addTo( map );
}

var showAllButton = L.easyButton({
  states: [{
    stateName: 'show-all',
    icon: 'fa-arrows-alt',
    title: 'Mostrar todos os pontos',
    onClick: function(btn, map) {
    	map.setView( [-15.7797200, -47.9297200], 4 );
    }
  }]
});
showAllButton.addTo( map );

/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "topleft",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-location-arrow",
  metric: false,
  strings: {
    title: "Minha posição",
    popup: "Você está a menos de {distance} {unit} deste ponto",
    outsideMapBoundsMsg: "Parece que você está localizado fora das fronteiras deste mapa"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);

/* About the map control */
var aboutControl = L.control({
  position: "topleft"
});
aboutControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control leaflet-bar leaflet-control-about");
  div.innerHTML = "<a title='Entenda o mapa' class='leaflet-control-part aboutControl' href='#' onclick='$(\"#aboutModal\").modal(\"show\"); return false;'><span class='fa fa-question-circle'></span><span id='aboutControlText'> Entenda o mapa</span></a>";
  return div;
};
map.addControl(aboutControl);

/* About the map control */
var downloadControl = L.control({
  position: "topleft"
});
downloadControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control leaflet-bar leaflet-control-about");
  //div.innerHTML = "<a title='Instale o aplicativo Responsa' href='https://play.google.com/store/apps/details?id=br.org.eita.responsa' target='_blank'><img src='http://consumoresponsavel.org.br/wp-content/uploads/2016/10/disponível-na-google-play-store.png' style='width:90px'</a>";
  return div;
};
map.addControl(downloadControl);





/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {};

var groupedOverlays = {
  "Iniciativas": {}
};
var layerControl = {};

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
	syncSidebar();
  if (typeof currentId != 'undefined' && currentId!=null) {
  	openFeatureInfoModal(currentId);
  	currentId=null;
  } else {
  	$("#loading").hide();
  }
  sizeLayerControl();
  /* Fit map to boroughs bounds */
  //map.fitBounds(theaters.getBounds());
  
  featureList = new List("features", {page:10000, valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var dataResponsaTitlesBH = new Bloodhound({
    name: "IniciativasTitles",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: dataResponsaSearch,
    limit: 10
  });
  var dataResponsaLocationsBH = new Bloodhound({
    name: "IniciativasLocations",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.location);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: dataResponsaSearch,
    limit: 10
  });

  dataResponsaTitlesBH.initialize();
  dataResponsaLocationsBH.initialize();
  

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Iniciativas",
    displayKey: "name",
    source: dataResponsaTitlesBH.ttAdapter(),
    templates: {
      //header: "<h4 class='typeahead-header'><img src='assets/img/theater.png' width='24' height='28'>&nbsp;Iniciativas</h4>",
      header: "<h4 class='typeahead-header'>Iniciativas</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{location}}</small>"].join(""))
    }
  }, {
    name: "Cidades",
    displayKey: "location",
    source: dataResponsaLocationsBH.ttAdapter(),
    templates: {
      //header: "<h4 class='typeahead-header'><img src='assets/img/theater.png' width='24' height='28'>&nbsp;Cidades</h4>",
      header: "<h4 class='typeahead-header'>Cidades</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{location}}</small>"].join(""))
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Boroughs") {
      map.fitBounds(datum.bounds);
    }
    if (datum.source === "Iniciativas") {
    	$.each(dataResponsaLayers, function (category, layer) {
		  	if (!map.hasLayer(layer)) {
		      map.addLayer(layer);
		    }
		    map.setView([datum.lat, datum.lng], 17);
		    if (map._layers[datum.id]) {
		      map._layers[datum.id].fire("click");
		    }
    	});
    }
    
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
  
});

// Leaflet patch to make layer control scrollable on touch browsers
// Has error. Disabled.
/*var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}*/


// Mail error check:
function addError (el) {
  return el.parent().addClass('has-error');
};
$('#contact-form').submit(function () {
  var hasError = false,
      name     = $('#form-name'),
      mail     = $('#form-email'),
      subject  = $('#form-subject'),
      message  = $('#form-message'),
      testmail = /^[^0-9][A-z0-9._%+-]+([.][A-z0-9_]+)*[@][A-z0-9_-]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/,
      $this    = $(this);

  $this.find('div').removeClass('has-error');

  if (name.val() === '') {
      hasError = true;
      addError(name);
  }

  if (!testmail.test(mail.val())) {
      hasError = true;
      addError(mail);
  }

  if (subject.val() === '') {
      hasError = true;
      addError(subject);
  }

  if (message.val() === '') {
      hasError = true;
      addError(message);
  }

  if (hasError === false) {
      return true;
  }

  return false;
});
