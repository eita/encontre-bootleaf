<?php

function curl_get($url, array $get = NULL, array $options = array())
{
		if ($get) {
			$url .= '?'.http_build_query($get);
		}
    $defaults = array(
        CURLOPT_HEADER => 0,
        CURLOPT_URL => $url,
        CURLOPT_FRESH_CONNECT => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FORBID_REUSE => 1,
        CURLOPT_TIMEOUT => 4,
    );
    
    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function echo_metadata($metadata=null, $scope="") {
        if (!$metadata) { return ""; }
	foreach ($metadata as $name=>$m) {
		$name_with_scope = ($scope) ? $scope.':'.$name : $name;
		if (is_object($m)) {
			echo_metadata($m, $name_with_scope);
		} else {
			?>
			<meta property="<?= $name_with_scope ?>" name="<?= $name_with_scope ?>" content="<?= str_replace('"', "'", $m) ?>" />
			<?php
		}
	}
}

?>
