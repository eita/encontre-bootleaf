<?php
require_once 'assets/configMail.php';
require_once 'vendor/autoload.php';
require_once "Mail.php";
require_once "Mail/mime.php";

$mailLoader   = new SplClassLoader('SimpleMail', './vendor');
$mailLoader->register();
use SimpleMail\SimpleMail;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name    = stripslashes(trim($_POST['form-name']));
    $email   = stripslashes(trim($_POST['form-email']));
    $phone   = stripslashes(trim($_POST['form-phone']));
    $subject = stripslashes(trim($_POST['form-subject']));
    $message = stripslashes(trim($_POST['form-message']));
    $pattern = '/[\r\n]|Content-Type:|Bcc:|Cc:/i';
    if (preg_match($pattern, $name) || preg_match($pattern, $email) || preg_match($pattern, $subject)) {
        die("Header injection detected");
    }

    $emailIsValid = filter_var($email, FILTER_VALIDATE_EMAIL);

    if ($name && $email && $emailIsValid && $subject && $message) {
        /*$mail = new SimpleMail();

        $mail->setTo($config->mailSettings['emails']['to']);
        $mail->setFrom($config->mailSettings['emails']['from']);
        $mail->setSender($name);
        $mail->setSubject($config->mailSettings['subject']['prefix'] . ' ' . $subject);
				*/
        $body = "
        <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
        <html>
            <head>
                <meta charset=\"utf-8\">
            </head>
            <body>
            		<p>introMessage</p>
            		<ul>
                	<li><strong>{$config->mailSettings['fields']['name']}:</strong> {$name}</li>
                	<li><strong>{$config->mailSettings['fields']['email']}:</strong> <a href='mailto:{$email}'>{$email}</a></li>
                	<li><strong>{$config->mailSettings['fields']['phone']}:</strong> {$phone}</li>
                	<li><strong>{$config->mailSettings['fields']['subject']}:</strong> {$subject}</li>
                	<li><strong>{$config->mailSettings['fields']['message']}:</strong> {$message}</li>
                </ul>
                <p><i>Esta mensagem foi enviada automaticamente a partir do formulário de contato no <a href='http://mapa.consumoresponsavel.org.br'><strong>Mapa do Consumo Responsável</strong></a>, uma plataforma do <a href='http://institutokairos.net'>Instituto Kairós</a> e da <a href='http://eita.org.br'>Cooperativa de Trabalho EITA</a>.</i></p>
            </body>
        </html>";
        $bodyToUser = str_replace('introMessage', $config->mailSettings['messages']['bodyIntroToUser'], $body);
        $bodyToResponsa = str_replace('introMessage', $config->mailSettings['messages']['bodyIntroToResponsa'], $body);
				/*
        $mail->setHtml($body);
        $ret = $mail->send();
        */
        $mimeparams=array();
				$mimeparams['text_encoding']="7bit";
				$mimeparams['head_charset']="UTF-8";
				$mimeparams['text_charset']="UTF-8";
				$mimeparams['html_charset']="UTF-8";
				
				$toUser = $email;
				$toResponsa = $config->mailSettings['emails']['to'];
				$headersToResponsa = array (
					'From' => $config->mailSettings['emails']['from'],
					'To' => $toResponsa,
					'Subject' => $config->mailSettings['subject']['prefix'] . ' ' . $subject
				);
				$headersToUser = array (
					'From' => $config->mailSettings['emails']['from'],
					'To' => $toUser,
					'Subject' => $config->mailSettings['subject']['prefix'] . ' ' . $subject
				);
				
				$smtp = Mail::factory(
					'smtp',
					array (
						'host' => $config->smtp_host,
						'auth' => true,
						'username' => $config->smtp_username,
						'password' => $config->smtp_password
					)
				);
		
				// To Responsa:
				$mimeToResponsa = new Mail_mime("\n");
				$mimeToResponsa->setHTMLBody($bodyToResponsa);
				$bodyToResponsa = $mimeToResponsa->get($mimeparams);
				$headersToResponsa = $mimeToResponsa->headers($headersToResponsa);
				
				$mailToResponsa = $smtp->send($toResponsa, $headersToResponsa, $bodyToResponsa);
				
				if (PEAR::isError($mailToResponsa)) {
		 			$hasMailError = true;
        	$config->mailSettings['messages']['error'] .= "Erro enviando mensagem ao Responsa: ".$mailToResponsa->getMessage();
        	return;
				} else {
					$emailSent = true;
				}
				

				// To User:
				$mimeToUser = new Mail_mime("\n");
				$mimeToUser->setHTMLBody($bodyToUser);
				$bodyToUser = $mimeToUser->get($mimeparams);
				$headersToUser = $mimeToUser->headers($headersToUser);
				
				$mailToUser = $smtp->send($toUser, $headersToUser, $bodyToUser);
				
				if (PEAR::isError($mailToUser)) {
		 			$hasMailError = true;
        	$config->mailSettings['messages']['error'] .= "Erro enviando mensagem ao usuário: ".$mailToUser->getMessage();
        	return;
				} else {
					$emailSent = true;
				}
				
        
    } else {
        $hasMailError = true;
    }
}
?>
