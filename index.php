<?php


// For debugging
/*
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
*/

require('assets/config.php');
require('assets/functions.php');

// Send e-mail
if (isset($_POST['form-email'])) {
	require('send_mail.php');
}

$id = $lat = $lng = $zoom = $metadata = null;
if (isset($_GET['id']) && $_GET['id']) {
	$id = $_GET['id'];
	$url = 'https://dtygel.vps2.eita.org.br/responsa_datamining/ajax_get_initiative_metadata.php';
	$params = array(
		'unique_id'=>$id
	);
	$metadata = curl_get($url, $params);
	$metadata = json_decode($metadata);
	$lat = $metadata->lat;
	$lng = $metadata->lng;
	$zoom = 17;
}
if (isset($_GET['lat']) && $_GET['lat']) {
	$lat = $_GET['lat'];
}
if (isset($_GET['lng']) && $_GET['lng']) {
	$lng = $_GET['lng'];
}
if (isset($_GET['zoom']) && $_GET['zoom']) {
	$zoom = $_GET['zoom'];
}
if (isset($_GET['sidebarIsEnabled'])) {
	$config->sidebarIsEnabled = ($_GET['sidebarIsEnabled']);
}
$selectedCategories = (isset($_GET['categories']) && $_GET['categories'])
    ? json_encode(explode(',', $_GET['categories']))
    : 'null';
if ($selectedCategories!='null' && isset($_GET['getAllData'])) {
    $getAllData = ($_GET['getAllData'])
        ? 'true'
        : 'false';
} else {
    $getAllData = 'true';
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include('meta.php'); ?>
    <title>Mapa - Portal do Consumo Responsável</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css">
    <link rel="stylesheet" href="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css">
    <link rel="stylesheet" href="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css">
    <link rel="stylesheet" href="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.css">
    <link rel="stylesheet" href="assets/leaflet-groupedlayercontrol/leaflet.groupedlayercontrol.css">
    <link rel="stylesheet" href="assets/leaflet-awesome/leaflet.awesome-markers.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/CliffCloud/Leaflet.EasyButton/f3f3136bda3937aa813c80a1a6c8c921c6df06ed/src/easy-button.css" />
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/responsa/style.css">

    <link rel="shortcut icon" href="http://www.consumoresponsavel.org.br/wp-content/uploads/2014/11/favicon.ico" />
				

  </head>

  <body>
  <nav id="main-menu-nav" class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed social-icons-button" data-toggle="collapse" data-target="#social-icons-collapse" aria-expanded="false">
					<span class="sr-only">Clique para compartilhar</span>
					<span class="glyphicon glyphicon-share-alt"></span>
				</button>
				<button type="button" class="navbar-toggle collapsed social-icons-button" data-toggle="collapse" data-target="#search-collapse" aria-expanded="false">
					<span class="sr-only">Buscar por nome ou cidade/UF</span>
					<span class="glyphicon glyphicon-search"></span>
				</button>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-collapse" aria-expanded="false">
					<span class="sr-only">Clique para ver menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="#">
        	<img alt="Brand" src="assets/responsa/logo_top.png">
      	</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="main-menu-collapse">
				<ul class="nav navbar-nav top-menu">
					<li><a href="http://www.consumoresponsavel.org.br">Portal</a></li>
					<li class="active"><a href="#">Mapa</a></li>
				<!--	<li><a href="http://noticias.consumoresponsavel.org.br">Notícias</a></li>-->
					<li><a href="https://biblioteca.consumoresponsavel.org.br">Biblioteca</a></li>
					<li><a href="https://www.consumoresponsavel.org.br/carta-politica">O que é</a></li>
					<li><a href="https://www.consumoresponsavel.org.br/sobre">Quem somos</a></li>
				</ul>
			</div>
			<!-- Social icons -->
			<div class="collapse navbar-collapse" id="social-icons-collapse">
				<ul class="nav navbar-nav">
				<li class="feed-rss"><a href="https://noticias.consumoresponsavel.org.br/?feed=atom" title="Feed RSS"><i class="fa fa-lg fa-rss"></i></a></li>
                    	        <li class="facebook"><a href="https://www.facebook.com/aplicativoresponsa/?fref=ts" title="Siga-nos no Facebook" target="_blank"><i class="fa fa-lg fa-facebook"></i></a></li>
                                <li class="instagram"><a href="https://www.instagram.com/_responsa/" title="Siga-nos no instagram" target="_blank"><i class="fa fa-lg fa-instagram"></i></a></li>
                                <li class="googleplus"><a href="https://soundcloud.com/user-589244400" title="Ouça-nos no SoundCloud!" target="_blank"><i class="fa fa-lg fa-soundcloud"></i></a></li>

				</ul>
			</div>
			<!-- Search -->
			<div class="collapse navbar-collapse" id="search-collapse">
				<form class="navbar-form navbar-right" role="search">
					<div class="form-group has-feedback">
						<input id="searchbox" type="text" placeholder="Buscar" class="form-control">
						<span id="searchicon" class="fa fa-search form-control-feedback"></span>
					</div>
				</form>
			</div>
		</div>
	</nav>

<header id="branding" role="banner">
	<div class="row defaultContentWidth clearfix">
		<div id="logo" class="col-lg-12">
				<a href="https://mapa.consumoresponsavel.org.br">
					<span>Mapa</span>
				</a>
		</div>
	</div>
</header><!-- #branding -->

    <div id="container">
      <div id="sidebar">
        <div class="sidebar-wrapper">
          <div class="panel panel-default" id="features">
            <div class="panel-heading">
              <h3 class="panel-title">Lista de iniciativas
              <button type="button" class="btn btn-xs btn-default pull-right" id="sidebar-hide-btn"><i class="fa fa-chevron-left"></i></button></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-8 col-md-8">
                  <input type="text" class="form-control search" placeholder="nome ou cidade..." />
                </div>
                <div class="col-xs-4 col-md-4">
                  <button type="button" class="btn btn-primary pull-right sort" data-sort="feature-name" id="sort-btn"><i class="fa fa-sort"></i>&nbsp;Ordenar</button>
                </div>
              </div>
            </div>
            <div class="sidebar-table">
              <table class="table table-hover" id="feature-list">
                <thead class="hidden">
                  <tr>
                    <th>Name</th>
                  <tr>
                  <tr>
                    <th>Chevron</th>
                  <tr>
                </thead>
                <tbody class="list"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div id="map"></div>
    </div>
    <div id="loading">
      <div class="loading-indicator">
        <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-info progress-bar-full"></div>
        </div>
      </div>
    </div>
    
		<?php if(!empty($emailSent) || !empty($hasMailError)): ?>
			<?php $formatTxt = (!empty($hasMailError)) ? "danger" : "success"; ?>
			<?php $message = (!empty($hasMailError)) ? $config->mailSettings['messages']['error'] : $config->mailSettings['messages']['success']; ?>
			<div class="modal fade" id="mailSentModal" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
							<div class="modal-header">
		          <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
		          <h4 class="modal-title">Formulário de contato</h4>
          	</div>
						<div class="modal-body">
							<div class="alert alert-<?= $formatTxt ?> text-center"><?= $message ?></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		<?php endif; ?>
    
    <div class="modal fade" id="featureModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title text-primary" id="feature-title"><img src="" /><span id="feature-span-title"></span><br /><span id="feature-span-territory"></span></h4>
          </div>
          <div class="modal-body" id="feature-info"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Mapa do Consumo Responsável</h4>
          </div>
          <div class="modal-body">
            <ul class="nav nav-tabs" id="aboutTabs">
              <li class="active"><a href="#about" data-toggle="tab"><i class="fa fa-question-circle"></i>&nbsp;Entenda o mapa</a></li>
              <li><a href="#sources" data-toggle="tab"><i class="fa fa-info-circle"></i>&nbsp;Fontes dos dados</a></li>
              <li><a href="#download" data-toggle="tab"><i class="fa fa-database"></i>&nbsp;Dados abertos</a></li>
              <li><a href="#contact" data-toggle="tab"><i class="fa fa-envelope"></i>&nbsp;Contato</a></li>
            </ul>
            <div class="tab-content" id="aboutTabsContent">
              <div class="tab-pane fade active in" id="about">
                <p>Seja bem-vinda/o ao Mapa do Consumo Responsável!</p>
                <div class="panel">
                  <p>Esse mapa permite ao usuário localizar grupos, empreendimentos, experiências e outras iniciativas do consumo responsável.</p>
                  <p>Foi organizado a partir de mapeamentos realizados por atores parceiros, consolidando um conjunto bastante amplo de experiências que promovem a agroecologia, segurança alimentar, economia solidária e o consumo responsável.</p>
                  <p>São mais de 3.000 pontos mapeados em todo o país, mapeados de <a href="#sources" data-toggle="tab">11 fontes diferentes de dados</a>.</p>
                  <p>Trata-se de uma iniciativa do Instituto Kairós pelo Consumo Responsável e da Cooperativa de Trabalho EITA (Educação, Informação e Tecnologia para Autogestão)</p>
                </div>
              </div>
              <div id="sources" class="tab-pane fade">
                <table class="table">
                	<!--<thead class="thead-inverse">
                		<th>Categoria</th>
                		<th>Fonte e descrição</th>
                		<th>Quem é responsável</th>
                	</thead>-->
                	<tbody>
                	<?php foreach ($config->sources as $catSlug=>$source) { ?>
                		<?php foreach ($source as $i=>$m) {?>
                			<tr>
		              			<?php if ($i==0) { ?>
		              				<td rowspan="<?= count($source) ?>" class="text-center">
		              					<img src="assets/img/<?= $config->categories[$catSlug][1] ?>"> <br />
		              					<?= $config->categories[$catSlug][0] ?>
		              				</td>
		              			<?php } ?>
                				<td>
                					<dt>
                						<a target="_blank" href="<?= $m[1] ?>"><span class="fa fa-globe"></span> <?= $m[0] ?></a>
                					</dt>
                					<dd>
                						<i><?= $m[2] ?></i> <br />
                						<b>Por: </b><?= $m[3] ?>
                					</dd>
                				</td>
                			</tr>
                		<?php } ?>
                	<?php } ?>
                	</tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="download">
                <p>Dados públicos para baixar</p>
                <div class="panel">
              		<p>Seguindo a filosofia do conhecimento livre e dos dados abertos, disponibilizamos abaixo todos os dados do mapa.</p>
              		<p>Os dados encontram-se no formato <a href="http://geojson.org/" target="_blank">GeoJson</a>.</p>
              		<p><a class="btn btn-success" data-dismiss="modal" data-window="external" href="<?= $config->dataDownloadUrl ?>?download=1">Baixar!</a></p>
              		
                </div>
              </div>
              <div class="tab-pane fade" id="contact">
                <form id="contact-form" action="index.php" method="post">
                  <div class="well well-sm">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group required">
                          <label for="form-name"><?= $config->mailSettings['fields']['name'] ?></label>:
                          <input type="text" class="form-control" id="form-name" name="form-name" required>
                        </div>
                        <div class="form-group required">
                          <label for="form-email"><?= $config->mailSettings['fields']['email'] ?></label>:
                          <input type="text" class="form-control" id="form-email" name="form-email" required>
                          
                        </div>
                        <div class="form-group">
                          <label for="form-phone"><?= $config->mailSettings['fields']['phone'] ?></label>:
                          <input type="text" class="form-control" id="form-email" name="form-phone">
                        </div>
                      </div>
                      <div class="col-md-8">
                      	<div class="form-group required">
                          <label for="form-subject"><?= $config->mailSettings['fields']['subject'] ?></label>:
                          <input type="text" class="form-control" id="form-subject" name="form-subject" required>
                        </div>
                        <div class="form-group required">
		                      <label for="form-subject"><?= $config->mailSettings['fields']['message'] ?></label>:
		                      <textarea class="form-control" rows="4" id="form-message" name="form-message" required></textarea>
		                    </div>
                      </div>
                      <div class="col-md-12">
                        <p>
                          <button type="submit" class="btn btn-primary pull-right"><?= $config->mailSettings['fields']['btn-send'] ?></button>
                        </p>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="attributionModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">
							Desenvolvido pela <a href='https://eita.coop.br'>Cooperativa de Trabalho <b>EITA</b></a>
            </h4>
          </div>
          <div class="modal-body">
            <div id="attribution">
            	<p>Uma iniciativa do <a href=''>Instituto Kairós de Consumo Responsável</a>.</p>
            	<p>Identidade visual por <a href=''>Estúdio Gunga'</a>.</p>
            	<p>Baseado no tema <i>bootleaf</i>, desenvolvido por  <a href='http://bryanmcbride.com'>bryanmcbride.com</a>.</p>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.5/typeahead.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script>
    <script src="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js"></script>
    <script src="https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.min.js"></script>
    <script src="assets/leaflet-groupedlayercontrol/leaflet.groupedlayercontrol.js"></script>
		<script src="assets/leaflet-awesome/leaflet.awesome-markers.js"></script>
		<script src="https://cdn.rawgit.com/CliffCloud/Leaflet.EasyButton/f3f3136bda3937aa813c80a1a6c8c921c6df06ed/src/easy-button.js"></script>

		<?php if ($id || ($lat && $lng && $zoom)) { ?>
			<script>
				<?php if ($lat) { ?>
					var currentLocation = {lat:<?= $lat ?>, lng:<?= $lng ?>};
				<?php } ?>
				<?php if ($zoom) { ?>
					var currentZoom = <?= $zoom ?>;
				<?php } ?>
				<?php if ($id) { ?>
					var currentId = "<?= $id ?>";
				<?php } ?>
			</script>
		<?php } ?>
		<?php 
			$categoryNames = "";
			$categoryIcons = "";
			foreach ($config->categories as $slug=>$cat) {
				$categoryNames .= "'$slug':'$cat[0]',";
				$categoryIcons .= "'$slug':'$cat[1]',";
			}
		?>
		<script>
			var sidebarIsEnabled = <?= $config->sidebarIsEnabled ?>;
			var selectedCategories = <?= $selectedCategories ?>;
	        var getAllData = <?= $getAllData ?>;
			var dataDownloadUrl = '<?= $config->dataDownloadUrl ?>';
			var categoryNames = {<?= $categoryNames ?>};
			var categoryIcons = {<?= $categoryIcons ?>};
			if (sidebarIsEnabled) {
				$('#sidebar').show();
			}
		</script>
    <script src="assets/js/app.js"></script>
  </body>
</html>
<?php
?>
