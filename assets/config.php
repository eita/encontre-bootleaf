<?php

$config = new StdClass();
$config->sidebarIsEnabled = 'false';

$config->dataDownloadUrl = 'https://dtygel.vps2.eita.org.br/responsa_datamining/ajax_get_all_initiatives_geojson.php';

$config->categories = array(
	"feiras"=>array("Feiras de orgânicos","feiras.png"),
	"iniciativas-de-economia-solidaria"=>array("Iniciativas de Economia Solidária","iniciativas_de_economia_solidaria.png"),
	"iniciativas-de-agroecologia"=>array("Iniciativas de Agroecologia","iniciativas_de_agroecologia.png"),
	"grupos-de-consumo-responsavel"=>array("Grupos de Consumo Responsável","grupos_de_consumo_responsavel.png"),
	"restaurantes"=>array("Restaurantes com ingredientes orgânicos","restaurantes.png"),
	"agricultura-urbana"=>array("Iniciativas de agricultura urbana","agricultura_urbana.png"),
	"coletivos-culturais"=>array("Coletivos culturais","coletivos_culturais.png"),
	"lojas"=>array("Lojas de consumo reponsável","lojas.png"),
);

$config->mailSettings = array(
    'subject' => array(
        'prefix' => '[Mapa do Consumo Responsável]'
    ),
    'messages' => array(
        'error'   => 'Houve um erro enviando a mensagem. Envie e-mail diretamente para contato@consumoresponsavel.org.br .',
        'success' => 'Sua mensagem foi enviada com sucesso.',
        'bodyIntroToUser' => 'Você enviou com sucesso uma mensagem para o <a href="http://mapa.consumoresponsavel.org.br">Mapa do Consumo Responsável</a>. Esperamos responder em breve! Seguem abaixo os dados que você enviou. Caso não tenha resposta em uma semana, você pode também escrever diretamente para <a href="mailto:contato@consumoresponsavel.org.br">contato@consumoresponsavel.org.br</a>.',
        'bodyIntroToResponsa' => 'O <a href="http://mapa.consumoresponsavel.org.br">Mapa do Consumo Responsável</a> recebeu um e-mail no formulário de contato. Seguem abaixo os dados enviados'
    ),
    'fields' => array(
        'name'     => 'Nome completo',
        'email'    => 'Email',
        'phone'    => 'Telefone',
        'subject'  => 'Assunto',
        'message'  => 'Messagem',
        'btn-send' => 'Enviar'
    )
);

$config->sources = array();

$config->sources['feiras'] = array(
	array(
		'Mapa de Feiras Orgânicas',
		'http://www.idec.org.br/feirasorganicas', 
		'Feiras orgânicas no país', 
		'IDEC – Instituto de Defesa do Consumidor', 
		'http://idec.org.br'
	)
);

$config->sources['grupos-de-consumo-responsavel'] = array(
	array(
		'Mapa dos Grupos de Consumo Responsável',
		'http://goo.gl/x7S3of', 
		'Grupos de consumo responsável no país', 
		'Instituto Kairós', 
		'http://institutokairos.net/'
	)
);

$config->sources['iniciativas-de-agroecologia'] = array(
	array(
		'Ideias na mesa', 
		'http://www.ideiasnamesa.unb.br/', 
		'Experiências de educação alimentar e nutricional no país', 
		'Coordenação Geral de Educação Alimentar e Nutricional do Ministério do Desenvolvimento Social e Combate à Fome (CGEAN/MDS) e do Observatório de Políticas de Segurança Alimentar e Nutrição – Universidade de Brasília (OPSAN/UnB)', 
		''
	),
	array(
		'Agroecologia em Rede', 
		'http://agroecologiaemrede.org.br', 
		'Experiências e pesquisas em agroecologia no país', 
		'Articulação Nacional de Agroecologia (ANA), Associação Brasileira de Agroecologia (ABA) e Sociedade Cientifica Latino-americana de Agroecologia (Socla)', 
		''
	),
	array(
		'Hortas de São Paulo',
		'https://www.google.com/maps/d/viewer?mid=1Jv4a5V6_DJq7c9OJSCLyF182LzU', 
		'Hortas comunitárias na cidade de São Paulo',
		'Instituto Kairós', 
		'http://institutokairos.net/'
	),
	array(
		'Roteiro EcoSampa', 
		'https://www.google.com/maps/d/viewer?mid=1-2XUw3Y3uQ9dFXHrNBG1nSHfvlM', 
		'Roteiros ecoturísticos na cidade de São Paulo', 
		'Instituto Kairós', 
		'http://institutokairos.net/'
	)
);

$config->sources['iniciativas-de-economia-solidaria'] = array(
	array(
		'CIRANDAS.net', 
		'http://cirandas.net', 
		'Empreendimentos econômico solidários de segmentos variados no país (vestuário, alimentação, artesanato, etc)', 
		'Fórum Brasileiro de Economia Solidária', 
		'http://fbes.org.br/'
	),
	array(
		'Rede COMSOL', 
		'https://goo.gl/nHxCOR', 
		'Pontos fixos de comercialização solidária, tais como lojas, feiras e centrais de comercialização, que comercializam produtos de segmentos variados no país (vestuário, alimentação, artesanato, etc)', 
		'Instituto Marista de Solidariedade', 
		'http://marista.edu.br/ims/'
	)
);

$config->sources['restaurantes'] = array(
	array(
		'Orgânicos no Prato',
		'http://www.organicosnoprato.com.br/',
		'Restaurantes que utilizam ingredientes orgânicos em seus cardápios na cidade de São Paulo',
		'Instituto Kairós', 
		'http://institutokairos.net/'
	)
);
?>
